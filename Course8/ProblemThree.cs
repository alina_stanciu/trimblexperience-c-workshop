﻿/*
   Create a program that prompts the user for the name of a text file. 
        the program will check that the file exists, displaying a message and quitting if it does not. 
        the file will be opened and copied to another file (prompt the user for the file name), but with every character converted to uppercase. 
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace Course8
{
    class ProblemThree
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the path for the file you are looking for:");
            string oldPathAndName = @"" + Console.ReadLine();

            if (!File.Exists(oldPathAndName))
            {
                Console.WriteLine("The file you are looking for doesn't exist in the specified location!");
                Thread.Sleep(3500);
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("The file you are looking for has been found!\n");
                Thread.Sleep(1300);

                //Read the file as one string & display the file contents to the console

                string fileContent = File.ReadAllText(oldPathAndName);
                Console.WriteLine("Contents of {0}: \n\n{1}\n", oldPathAndName, fileContent);

                /* 
                 * another method
                 * 
                using (FileStream fs = File.Open(filePath, FileMode.Open))
                {
                    byte[] b = new byte[1024];
                    UTF8Encoding temp = new UTF8Encoding(true);

                    while (fs.Read(b, 0, b.Length) > 0)
                    {
                        Console.WriteLine(temp.GetString(b));
                    }
                }
                */

                Thread.Sleep(2000);
                Console.WriteLine("\nLet's copy your file and convert every character to uppercase!");
                Thread.Sleep(1300);
                Console.WriteLine("\nEnter new path (if desired) and new file name:");
                string newPathAndName = @"" + Console.ReadLine();

                File.Copy(oldPathAndName, newPathAndName);

                File.WriteAllText(newPathAndName, fileContent.ToUpper());

                Thread.Sleep(1000);
                Console.WriteLine("\nFile succesfully copied and renamed with all characters converted to uppercase!");

                Console.ReadKey();
            }
        }
    }
}