﻿/*
   Build a method that accepts a list of professors or students and serializes that list as XML locally (xml file creation). + JSON
        The file should be overwritten if it already exists when the method is called.

    Build another method that deserializes the xml file and binds the result to a List type variable(s): students, teachers (read from xml students and teachers)
*/

using System;
using System.Collections.Generic;
using System.Threading;

namespace Course8
{
    class ProblemsOneTwo
    {
        static void Main(string[] args)
        {
            List<Teacher> teachersList = new List<Teacher>();
            teachersList.Add(new Teacher("Ellen", "Pompeo", 46, 1, "Stanford University", 18, "Biology"));
            teachersList.Add(new Teacher("Patrick", "Dempsey", 51, 2, "Oxford University", 26, "Mathematics"));
            teachersList.Add(new Teacher("Kevin", "McKidd", 48, 3, "Ohio State University", 20, "Literature"));
            teachersList.Add(new Teacher("Camilla", "Luddington", 54, 4, "University of Central Florida", 12, "Modern History"));
            
            Serializer<Teacher>.SerializeXml(teachersList, "teachers.xml");
            Thread.Sleep(2000);
            Console.WriteLine("XML serialization complete!");

            List<Teacher> newTeachersXml = Serializer<Teacher>.DeserializeXml("teachers.xml");
            Thread.Sleep(2000);
            Console.WriteLine("\nXML deserialization complete!");

            foreach (Teacher teacher in newTeachersXml)
            {
                Console.WriteLine(teacher.ToString());
            }

            List<Student> studentsList = new List<Student>();
            studentsList.Add(new Student("Elliot", "Anderson", 21, 1, 10, 2, "Computer Science"));
            studentsList.Add(new Student("Tyrel", "Wellick", 23, 2, 8, 4, "Bussiness Administration"));
            studentsList.Add(new Student("Darlene", "Kurta", 19, 3, 7, 1, "Filology"));
            studentsList.Add(new Student("Dominique", "Alvez", 22, 4, 4, 3, "Mathematics"));
            studentsList.Add(new Student("Ozgecan", "Ayili", 20, 5, 6, 1, "Filology"));
            studentsList.Add(new Student("Johannes", "Spaas", 24, 6, 10, 6, "Medicine"));

            Serializer<Student>.SerializeJson(studentsList, "students.json");
            Thread.Sleep(2000);
            Console.WriteLine("\nJSON serialization complete!");

            List<Student> newStudentsJson = Serializer<Student>.DeserializeJson("students.json");
            Thread.Sleep(2000);
            Console.WriteLine("\nJSON deserialization complete!");

            foreach (Student student in newStudentsJson)
            {
                Console.WriteLine(student.ToString());
            }

            Console.ReadKey();
        }
    }
}