﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course8
{
    public abstract class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public int Age { get; set; }

        public int ID { get; set; }

        public Person (string Name, string Surname, int Age, int ID)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Age = Age;
            this.ID = ID;
        }

        public Person()
        {
        }

        public override string ToString()
        {
            return $"\n{nameof(Name)}: {Name}, {nameof(Surname)}: {Surname}, {nameof(Age)}: {Age}, {nameof(ID)}: {ID}";
        }

        public abstract string GetDetail();
    }
}