﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Course8
{
    class Serializer<T>
    {
        public static void SerializeXml(List<T> input, string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute("File"));
            TextWriter writer = new StreamWriter(fileName);
            serializer.Serialize(writer, input);

            writer.Close();
        }

        public static List<T> DeserializeXml(string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute("File"));
            TextReader reader = new StreamReader(fileName);

            List<T> input = (List<T>)serializer.Deserialize(reader);
            reader.Close();

            return input;
        }

        public static void SerializeJson(List<T> input, string fileName)
        {
            string json = JsonSerializer.Serialize(input, typeof(List<T>));
            File.WriteAllText(fileName, json);
        }

        public static List<T> DeserializeJson(string fileName)
        {
            string json = File.ReadAllText(fileName);
            List<T> input = JsonSerializer.Deserialize<List<T>>(json);

            return input;
        }
    }
}
