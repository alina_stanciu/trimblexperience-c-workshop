﻿/* 
   Write a C# program with the following requirements:
    a. calculate the area of a circle
    b. calculate the area of a rectangle
    c. calculate the area of a cylinder 
    
    Calculations should be done based on the option entered and with the dimensions provided by the user from the keyboard.
    Each area calculation should be done using the type of methods presented in this course.
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Course4
{
    class Homework
    {
        private static void CircleArea()
        {
            Console.WriteLine("\nEnter the radius of the circle: ");
            double circleRadius = Convert.ToDouble(Console.ReadLine());           
            
            double circleArea = Math.PI * Math.Pow(circleRadius, 2);            

            Console.WriteLine($"The area of the circle with the entered radius is {circleArea:0.##}\n");
        }

        private static void RectangleArea()
        {
            Console.Write("\nEnter the length of the rectangle: ");
            double length = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter the width of the rectangle: ");
            double width = Convert.ToDouble(Console.ReadLine());

            double rectangleArea = length * width;

            Console.WriteLine($"The area of the rectangle with the entered values for length and widths is {rectangleArea:0.##}\n");
        }

        private static void CylinderSurfaceArea()
        {
            Console.Write("\nEnter the radius of the cylinder: ");
            double radius = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter the height of the cylinder: ");
            double height = Convert.ToDouble(Console.ReadLine());

            double cylinderSurfaceArea = 2 * Math.PI * radius * (height + radius);

            Console.WriteLine($"The surface area of the cylinder with the entered values for radius and height is {cylinderSurfaceArea:0.##}\n");
        }

        static void Main(string[] args)
        {
            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Calculate the area of a circle.");
                Console.WriteLine("\t2. Calculate the area of a rectangle.");
                Console.WriteLine("\t3. Calculate the surface area of a cylinder");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                switch (choice)
                {
                    case '1':
                        CircleArea();
                        break;
                    case '2':
                        RectangleArea();
                        break;
                    case '3':
                        CylinderSurfaceArea();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }        
    }
}