﻿//Write a program that counts user specified symbols in a string read as input.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemThree
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string: ");
            string userString = Console.ReadLine();

            Console.Write("Enter the symbol to be counted: ");
            char symbol = char.Parse(Console.ReadLine());

            int count = 0;

            foreach (char character in userString)
            {
                if (character == symbol)
                {
                    count++;
                }
            }
            Console.WriteLine("The symbol " + symbol + " appears " + count + " time(s) in the given string.");
            Console.ReadKey();
        }
    }
}