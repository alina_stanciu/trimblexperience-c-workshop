﻿//Write a program that counts user specified symbols in a string read as input.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemThree_v3
    {
        static int CountSymbol(string userString, char symbol)
        {
            int index = 0;
            int count = 0;

            while (index < userString.Length)
            {
                if (userString[index] == symbol)
                {
                    count++;
                }
                index++;
            }
            return count;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string: ");
            string userString = Console.ReadLine();

            Console.Write("Enter the symbol to be counted: ");
            char symbol = char.Parse(Console.ReadLine());

            Console.WriteLine("The symbol " + symbol + " appears " + CountSymbol(userString, symbol) + " time(s) in the given string.");

            Console.ReadKey();
        }
    }
}
