﻿//Write a program that specifies the odd and even numbers between 0 and a value entered by the user.


using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Course2
{
    class ProblemOne_v2
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
            int userInput = Int32.Parse(Console.ReadLine());

            List<int> myList = new List<int>();

            for (int currentNumber = 0; currentNumber <= userInput; currentNumber++)
            {
                myList.Add(currentNumber);
            }

            Console.WriteLine("\nThe list is:");
            string list = string.Join(",", myList);
            Console.Write(list);

            Console.WriteLine();

            Console.WriteLine("\nAre the numbers in the list even?");
            var bEvenList = myList.Select(x => x % 2 == 0).ToList();
            string bEvenResult = string.Join(",", bEvenList);
            Console.Write(bEvenResult);

            Console.WriteLine();

            Console.WriteLine("\nAre the numbers in the list odd?");
            var bOddList = myList.Select(x => x % 2 != 0).ToList();
            string bOddResult = string.Join(",", bOddList);
            Console.Write(bOddResult);

            Console.WriteLine();

            var evenList = (from number in myList where number % 2 == 0 select number).ToList();
            var oddList = (from number in myList where number % 2 != 0 select number).ToList();

            Console.WriteLine("\nPrinting even numbers:");
            string evenResult = string.Join(",", evenList);
            Console.Write(evenResult);

            Console.WriteLine();

            Console.WriteLine("\nPrinting odd numbers:");
            string oddResult = string.Join(",", oddList);
            Console.Write(oddResult);
            
            Console.ReadKey();
        }
    }
}
