﻿//Write a program that checks if a string is palindrome(= strings that can be read equally from right to left, such as 'kayak', ‘ANNA’).


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemFour
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string:");
            string userString = Console.ReadLine();

            string reversedString = "";

            for (int index = userString.Length - 1; index >= 0; index--)
            {
                reversedString += userString[index].ToString();
            }

            Console.WriteLine("The entered string is {0} and the reversed string is {1}.", userString.ToLower(), reversedString.ToLower());

            if (reversedString.Equals(userString, StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("The string is a palindrome!");
            }
            else
            {
                Console.WriteLine("The string is NOT a palindrome!");
            }
            Console.ReadKey();
        }
    }
}