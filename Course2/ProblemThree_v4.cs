﻿//Write a program that counts user specified symbols in a string read as input.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemThree_v4
    {
        static int CountSymbol(string userString, char symbol)
        {
            int count = 0;
            int index = 0;

            do
            {
                if (userString[index] == symbol)
                {
                    count++;
                }
                index++;
            } while (index < userString.Length);

            return count;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string: ");
            string userString = Console.ReadLine();

            Console.Write("Enter the symbol to be counted: ");
            char symbol = char.Parse(Console.ReadLine());

            Console.WriteLine("The symbol " + symbol + " appears " + CountSymbol(userString, symbol) + " time(s) in the given string.");

            Console.ReadKey();
        }
    }
}
