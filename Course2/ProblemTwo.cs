﻿//Write a program that specifies if a grade is excellent, very good, good, average or failed. The program should be implemented using switch statement.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemTwo
    {
        static void Main(string[] args)
        {
            Console.Write("Enter grade between 0 and 10: ");
            double grade = double.Parse(Console.ReadLine());

            char newGrade = '0';

            if (grade >= 0 && grade < 5)
            {
                newGrade = 'F';
            }
            else if (grade >= 5 && grade < 7)
            {
                newGrade = 'A';
            }
            else if (grade >= 7 && grade < 9)
            {
                newGrade = 'G';
            }
            else if (grade >= 9 && grade < 10)
            {
                newGrade = 'V';
            }
            else if (grade == 10)
            {
                newGrade = 'E';
            }


            switch (newGrade)
            {
                case 'F':
                    Console.WriteLine("Result: FAILED");
                    break;
                case 'A':
                    Console.WriteLine("Result: AVERAGE");
                    break;
                case 'G':
                    Console.WriteLine("Result: GOOD");
                    break;
                case 'V':
                    Console.WriteLine("Result: VERY GOOD");
                    break;
                case 'E':
                    Console.WriteLine("Result: EXCELLENT");
                    break;
                default:
                    Console.WriteLine("Result: ERROR");
                    break;
            }
            Console.ReadKey();
        }
    }
}