﻿//Write a program that specifies the odd and even numbers between 0 and a value entered by the user.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course2
{
    class ProblemOne
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
            int userInput = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Printing all even numbers between 0 and " + userInput + ":");

            for (int currentNumber = 0; currentNumber <= userInput; currentNumber += 2)
            {
                Console.WriteLine(currentNumber);
            }

            Console.WriteLine("Printing all odd numbers between 0 and " + userInput + ":");

            for (int currentNumber = 1; currentNumber <= userInput; currentNumber += 2)
            {
                Console.WriteLine(currentNumber);
            }
            Console.ReadKey();
        }
    }
}