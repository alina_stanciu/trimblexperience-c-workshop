﻿/*Create a console app that checks whether a triangle can be formed by the entered angle values 
and prints whether the triangle is equilateral, isosceles or scalene based on the entered side length values.*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Course1
{
    class ProblemThree
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the first angle of the triangle: ");
            int firstAngle = Int32.Parse(Console.ReadLine());

            Console.Write("Enter the second angle of the triangle: ");
            int secondAngle = Int32.Parse(Console.ReadLine());

            Console.Write("Enter the third angle of the triangle: ");
            int thirdAngle = Int32.Parse(Console.ReadLine());

            int anglesSum = firstAngle + secondAngle + thirdAngle;

            if (anglesSum == 180)
            {
                if (firstAngle == secondAngle && secondAngle == thirdAngle)
                {
                    Console.WriteLine("The triangle with the entered angles is EQUILATERAL.");
                }
                else if (firstAngle == secondAngle || secondAngle == thirdAngle || firstAngle == thirdAngle)
                {
                    Console.WriteLine("The traingle with the entered angles is ISOSCELES.");
                }
                else
                {
                    Console.WriteLine("The triangle with the entered angles is SCALENE.");
                }
            }
            else
            {
                Console.WriteLine("Error! The sum of the angles of a triangle must be 180 degrees.");
            }
            Console.ReadKey();
        }
    }
}