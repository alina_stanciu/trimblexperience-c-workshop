﻿//Create a console app that can calculate the surface of a circle. 
//Note: The radius of the circle should be read from keyboard input.


using System;

namespace Course1
{
    class ProblemOne
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the radius of the circle: ");

            double circleRadius = double.Parse(Console.ReadLine());

            const double pi = Math.PI;

            double circleArea = pi * Math.Pow(circleRadius, 2);

            Console.WriteLine("The area of the circle with the entered radius is: " + circleArea);

            Console.ReadKey();

        }
    }
}