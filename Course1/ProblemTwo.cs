﻿//Create a console app that converts Celsius degrees to Kelvin and Fahrenheit and vice-versa.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course1
{
    class ProblemTwo
    {
        static void FromCelsius()
        {
            Console.Write("Enter the temperature in Celsius degrees: ");
            double celsius = double.Parse(Console.ReadLine());

            double kelvin = celsius + 273.15;

            double fahrenheit = (celsius * 9) / 5 + 32;

            Console.WriteLine("The temperature in Kelvin is: " + kelvin);
            Console.WriteLine("The temperature in Fahrenheit degrees is: " + fahrenheit);
        }

        static void ToCelsius()
        {
            Console.Write("Enter the temperature in Kelvin: ");
            double kelvin = double.Parse(Console.ReadLine());
            double celsiusFromKelvin = kelvin - 273.15;
            Console.WriteLine("From Kelvin to Celsius degrees: " + celsiusFromKelvin);

            Console.Write("Enter the temperature in Fahrenheit degrees: ");
            double fahrenheit = double.Parse(Console.ReadLine());
            double celsiusFromFahrenheit = (5 * fahrenheit - 160) / 9;
            Console.WriteLine("From Fahrenheit degrees to Celsius degrees: " + celsiusFromFahrenheit);
        }
        static void Main(string[] args)
        {
            FromCelsius();

            ToCelsius();

            Console.ReadKey();
        }

    }
}