﻿/*
   Create a C# program : 
    1. That reads an integer day number(between 1 and 365) from the console and converts this number into a month and a day of the month and then prints the result to the console. 
    2. Detects whether the specified year from the console is a leap year. 
*/


using System;

namespace Course3
{
    class Homework
    {
        static void Main(string[] args)
        {
            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Convert a specified number into a month and day.");
                Console.WriteLine("\t2. Detect if a specified year is a leap year.");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                switch (choice)
                {
                    case '1':
                        DateCalculator();
                        break;
                    case '2':
                        LeapYearCalculator();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }         
        }

        public static void DateCalculator()
        {
            Console.Write("\nEnter a number between 1 and 365: ");
            int number;

            try
            {
                number = Convert.ToInt32(Console.ReadLine());
            }

            catch (FormatException invalidFormat)
            {
                Console.WriteLine("ERROR: {0}", invalidFormat.Message);
                throw invalidFormat;
            }

            try
            {
                if (number < 1 || number > 365)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException outOfRange)
            {
                Console.WriteLine("ERROR: {0}", outOfRange.Message);
                throw outOfRange;
            }

            if (number >= 1 && number <= 31)
            {
                Console.WriteLine("January " + number);
            }
            if (number > 31 && number <= 59)
            {
                Console.WriteLine("February " + (number - 31) + "\n");
            }
            if (number > 59 && number <= 90)
            {
                Console.WriteLine("March " + (number - 59) + "\n");
            }
            if (number > 90 && number <= 120)
            {
                Console.WriteLine("April " + (number - 90) + "\n");
            }
            if (number > 120 && number <= 151)
            {
                Console.WriteLine("May " + (number - 120) + "\n");
            }
            if (number > 151 && number <= 181)
            {
                Console.WriteLine("June " + (number - 151) + "\n");
            }
            if (number > 181 && number <= 212)
            {
                Console.WriteLine("July " + (number - 181) + "\n");
            }
            if (number > 212 && number <= 243)
            {
                Console.WriteLine("August " + (number - 212) + "\n");
            }
            if (number > 243 && number <= 273)
            {
                Console.WriteLine("September " + (number - 243) + "\n");
            }
            if (number > 273 && number <= 304)
            {
                Console.WriteLine("October " + (number - 273) + "\n");
            }
            if (number > 304 && number <= 334)
            {
                Console.WriteLine("November " + (number - 304) + "\n");
            }
            if (number > 334 && number <= 365)
            {
                Console.WriteLine("December " + (number - 334) + "\n");
            }
        }

        public static void LeapYearCalculator()
        {
            Console.Write("\nEnter a number representing a year: ");
            int year;          

            try
            {               
                year = Convert.ToInt32(Console.ReadLine());              
            }
            
            catch (FormatException invalidFormat)
            {
                Console.WriteLine("ERROR: {0}", invalidFormat.Message);
                throw invalidFormat;
            }

            try
            {
                if (year < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException outOfRange)
            {
                Console.WriteLine("ERROR: {0}", outOfRange.Message);
                throw outOfRange;
            }

            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
            {
                Console.WriteLine("The year is a leap year.\n");
            }
            else
            {
                Console.WriteLine("The year isn't a leap year.\n");
            }
        }
    }
}