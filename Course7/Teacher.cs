﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course7
{
    public class Teacher: Person //derived class - child
    {
        public string College;
        public int WorkingHours;
        public string Subject;

        public Teacher(string Name, string Surname, int Age, int ID, string College, int WorkingHours, string Subject): base(Name, Surname, Age, ID)
        {
            this.College = College;
            this.WorkingHours = WorkingHours;
            this.Subject = Subject;
        }

        public override string ToString()
        {
            return $"\n{base.ToString()}, {nameof(College)}: {College}";
        }

        public void CareerPath()
        {
            Console.WriteLine($"\n{Name} {Surname} is learning for his/her PHD.", Name, Surname);
        }

        public override string GetDetail()
        {
            return $"\n{base.ToString()}, {nameof(WorkingHours)}: {WorkingHours}, {nameof(Subject)}: {Subject}";
        }
    }
}