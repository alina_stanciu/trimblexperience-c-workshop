﻿using System;
using System.Collections.Generic;
using System.Text;
using Course7;

namespace Course7
{
    public class FacultyManager: IFacultyManager
    {
        public List<Teacher> ReturnTeachers()
        {
            List<Teacher> teachersList = new List<Teacher>();

            teachersList.Add(new Teacher("Ellen", "Pompeo", 46, 1, "Stanford University", 18, "Biology"));
            teachersList.Add(new Teacher("Patrick", "Dempsey", 51, 2, "Oxford University", 26, "Mathematics"));
            teachersList.Add(new Teacher("Kevin", "McKidd", 48, 3, "Ohio State University", 20, "Literature"));
            teachersList.Add(new Teacher("Camilla", "Luddington", 54, 4, "University of Central Florida", 12, "Modern History"));

            return teachersList;
        }

        public List<Student> ReturnStudents()
        {
            List<Student> studentsList = new List<Student>();

            studentsList.Add(new Student("Elliot", "Anderson", 21, 1, 10, 2, "Computer Science"));
            studentsList.Add(new Student("Tyrel", "Wellick", 23, 2, 8, 4, "Bussiness Administration"));
            studentsList.Add(new Student("Darlene", "Kurta", 19, 3, 7, 1, "Filology"));
            studentsList.Add(new Student("Dominique", "Alvez", 22, 4, 4, 3, "Mathematics"));
            studentsList.Add(new Student("Ozgecan", "Ayili", 20, 5, 6, 1, "Filology"));
            studentsList.Add(new Student("Johannes", "Spaas", 24, 6, 10, 6, "Medicine"));

            return studentsList;
        }
    }
}
