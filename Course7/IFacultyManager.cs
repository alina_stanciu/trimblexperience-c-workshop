﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course7
{
    interface IFacultyManager
    {
        List<Student> ReturnStudents();
        List<Teacher> ReturnTeachers();
    }
}
