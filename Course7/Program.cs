﻿/*
    Define a GetDetail method for the Person class:
        extend this method for the Teacher and Student classes
        for Teacher: return working hours and subject
        for Student: year of study, study program

    Create a class called FacultyManager that implements the interface IFacultyManager.

    The interface should have the signature of two methods. 
    The methods should return the lists of teachers and students as objects and the output should be displayed in console
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Course7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Teacher> teachersList = new List<Teacher>();
            teachersList.Add(new Teacher("Ellen", "Pompeo", 46, 1, "Stanford University", 18, "Biology"));
            teachersList.Add(new Teacher("Patrick", "Dempsey", 51, 2, "Oxford University", 26, "Mathematics"));
            teachersList.Add(new Teacher("Kevin", "McKidd", 48, 3, "Ohio State University", 20, "Literature"));
            teachersList.Add(new Teacher("Camilla", "Luddington", 54, 4, "University of Central Florida", 12, "Modern History"));

            List<Student> studentsList = new List<Student>();
            studentsList.Add(new Student("Elliot", "Anderson", 21, 1, 10, 2, "Computer Science"));
            studentsList.Add(new Student("Tyrel", "Wellick", 23, 2, 8, 4, "Bussiness Administration"));
            studentsList.Add(new Student("Darlene", "Kurta", 19, 3, 7, 1, "Filology"));
            studentsList.Add(new Student("Dominique", "Alvez", 22, 4, 4, 3, "Mathematics"));
            studentsList.Add(new Student("Ozgecan", "Ayili", 20, 5, 6, 1, "Filology"));
            studentsList.Add(new Student("Johannes", "Spaas", 24, 6, 10, 6, "Medicine"));

            FacultyManager facultyManager = new FacultyManager();
            List<Student> anotherStudentsList = facultyManager.ReturnStudents();
            List<Teacher> anotherTeachersList = facultyManager.ReturnTeachers();

            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Display teachers.");
                Console.WriteLine("\t2. Display students.");
                Console.WriteLine("\t3. Career paths for teachers.");
                Console.WriteLine("\t4. Career paths for students.");
                Console.WriteLine("\t5. More details about teachers.");
                Console.WriteLine("\t6. More details about students.");
                Console.WriteLine("\t7. Return all teachers and students.");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                switch (choice)
                {
                    case '1':
                        foreach (Teacher teacher in teachersList)
                        {
                            Console.WriteLine(teacher.ToString());
                        }
                        Console.WriteLine();
                        break;
                    case '2':
                        foreach (Student student in studentsList)
                        {
                            Console.WriteLine(student.ToString());
                        }
                        Console.WriteLine();
                        break;
                    case '3':
                        teachersList[0].CareerPath();
                        teachersList[1].CareerPath();
                        teachersList[2].CareerPath();
                        teachersList[3].CareerPath();
                        Console.WriteLine();
                        break;
                    case '4':
                        studentsList[0].CareerPath();
                        studentsList[1].CareerPath();
                        studentsList[2].CareerPath();
                        studentsList[3].CareerPath();
                        studentsList[4].CareerPath();
                        studentsList[5].CareerPath();
                        Console.WriteLine();
                        break;
                    case '5':
                        Console.WriteLine(teachersList[0].GetDetail());
                        Console.WriteLine(teachersList[1].GetDetail());
                        Console.WriteLine(teachersList[2].GetDetail());
                        Console.WriteLine(teachersList[3].GetDetail());
                        Console.WriteLine();
                        break;
                    case '6':
                        Console.WriteLine(studentsList[0].GetDetail());
                        Console.WriteLine(studentsList[1].GetDetail());
                        Console.WriteLine(studentsList[2].GetDetail());
                        Console.WriteLine(studentsList[3].GetDetail());
                        Console.WriteLine(studentsList[4].GetDetail());
                        Console.WriteLine(studentsList[5].GetDetail());
                        Console.WriteLine();
                        break;
                    case '7':
                        Console.WriteLine(anotherTeachersList[0]);
                        Console.WriteLine(anotherTeachersList[1]);
                        Console.WriteLine(anotherTeachersList[2]);
                        Console.WriteLine(anotherTeachersList[3]);
                        Console.WriteLine(anotherStudentsList[0]);
                        Console.WriteLine(anotherStudentsList[1]);
                        Console.WriteLine(anotherStudentsList[2]);
                        Console.WriteLine(anotherStudentsList[3]);
                        Console.WriteLine(anotherStudentsList[4]);
                        Console.WriteLine(anotherStudentsList[5]);
                        Console.WriteLine();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}