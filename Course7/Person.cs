﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course7
{
    public abstract class Person //base class - parent
    {
        public string Name;

        public string Surname;

        public int Age;

        public int ID;

        public Person (string Name, string Surname, int Age, int ID)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Age = Age;
            this.ID = ID;
        }

        public override string ToString()
        {
            return $"\n{nameof(Name)}: {Name}, {nameof(Surname)}: {Surname}, {nameof(Age)}: {Age}, {nameof(ID)}: {ID}";
        }

        public abstract string GetDetail();
    }
}