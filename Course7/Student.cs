﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course7
{
    public class Student : Person //derived class - child
    {
        public int Grade;
        public int YearOfStudy;
        public string StudyProgram;

        public Student (string Name, string Surname, int Age, int ID, int Grade, int YearOfStudy, string StudyProgram): base(Name, Surname, Age, ID)
        {
            this.Grade = Grade;
            this.YearOfStudy = YearOfStudy;
            this.StudyProgram = StudyProgram;
        }

        public override string ToString()
        {
            return $"\n{base.ToString()}, {nameof(Grade)}: {Grade}";
        }

        public void CareerPath()
        {
            Console.WriteLine($"\n{Name} {Surname} is learning for his/her Bachelor's Degree.", Name, Surname);
        }

        public override string GetDetail()
        {
            return $"\n{base.ToString()}, {nameof(YearOfStudy)}: {YearOfStudy}, {nameof(StudyProgram)}: {StudyProgram}"; 
        }
    }
}