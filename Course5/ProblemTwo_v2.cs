﻿//Write a program that multiplies two matrices with the square size 4:4.


using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace Course5
{
    class ProblemTwo_v2
    {
        static void MatrixMultiplication()
        {
            Matrix4x4 firstMatrix = new Matrix4x4(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            Matrix4x4 secondMatrix = new Matrix4x4(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            
            Matrix4x4 multiplicationMatrix = Matrix4x4.Multiply(firstMatrix, secondMatrix);

            Console.WriteLine(multiplicationMatrix);
        }

        static void Main(string[] args)
        {
            MatrixMultiplication();

            Console.ReadKey();
        }
    }
}