﻿//Create an identity matrix with the square size 15:15 and display it.


using System;

namespace Course5
{
    class ProblemOne
    {
        static void Identity(int size)
        {
            int row, column;

            for (row = 0; row < size; row++)
            {
                for (column = 0; column < size; column++)
                {
                    if (row == column)
                    {
                        Console.Write(1 + " ");
                    }
                    else
                    {
                        Console.Write(0 + " ");
                    }
                }
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            Console.Write("Enter the desired dimensions for your identity matrix: ");
            int size;

            try
            {
                size = Convert.ToInt32(Console.ReadLine()); //any other value other than 15 can be used as input
            }
            catch (FormatException invalidFormat)
            {
                Console.WriteLine("ERROR: {0}", invalidFormat.Message);
                throw invalidFormat;
            }
            try
            {
                if (size < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException outOfRange)
            {
                Console.WriteLine("ERROR: {0}", outOfRange.Message);
                throw outOfRange;
            }

            Console.WriteLine("The identity matrix with the desired dimensions is:");
            Identity(size);

            Console.ReadKey();
        }
    }
}