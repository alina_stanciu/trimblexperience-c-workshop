﻿//Write a program that randomly decides a winner out of 10 competitors and displays the name of the first two.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course5
{
    class ProblemThree
    {
        static void Main(string[] args)
        {
            var random = new Random();

            var namesList = new List<string>
                            {"Meredith Grey",
                            "Derek Sheperd",
                            "Miranda Bailey",
                            "Richard Webber",
                            "Alex Karex",
                            "Cristina Yang",
                            "Amelia Sheperd",
                            "Owent Hunt",
                            "Teddy Altman",
                            "Jo Wilson" };

            var winnersList = new List<string>();

            Console.WriteLine("The first two winners are: ");

            do
            {
                int randomIndex = random.Next(namesList.Count);

                while (!winnersList.Contains(namesList[randomIndex]))
                {
                    winnersList.Add(namesList[randomIndex]);
                    Console.WriteLine(namesList[randomIndex]);
                }
            } while (winnersList.Count < 2);

            Console.ReadKey();
        }
    }
}