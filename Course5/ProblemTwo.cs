﻿//Write a program that multiplies two matrices with the square size 4:4.


using System;
using System.Collections.Generic;
using System.Text;

namespace Course5
{
    class ProblemTwo
    {
        static void Main(string[] args)
        {
            int rowsIndex, columnsIndex;

            //the program can actually multiply matrices of any compatible sizes, not just 4x4

            Console.Write("Enter the number of rows for the first matrix: ");
            int rowsFirstMatrix = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the number of columns for the first matrix: ");
            int columnsFirstMatrix = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the number of rows for the second matrix: ");
            int rowsSecondMatrix = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the number of columns for the second matrix: ");
            int columnsSecondMatrix = Convert.ToInt32(Console.ReadLine());

            if (columnsFirstMatrix != rowsSecondMatrix)
            {
                Console.WriteLine("Matrix multiplication is not possible for the provided values for rows and columns!");
            }
            else
            {
                Console.WriteLine("Enter the first matrix:");
                int[,] firstMatrix = new int[rowsFirstMatrix, columnsFirstMatrix];

                for (rowsIndex = 0; rowsIndex < rowsFirstMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsFirstMatrix; columnsIndex++)
                    {
                        firstMatrix[rowsIndex, columnsIndex] = int.Parse(Console.ReadLine());
                    }
                }

                Console.WriteLine("The first matrix is:");

                for (rowsIndex = 0; rowsIndex < rowsFirstMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsFirstMatrix; columnsIndex++)
                    {
                        Console.Write(firstMatrix[rowsIndex, columnsIndex] + " ");
                    }
                    Console.WriteLine();
                }

                Console.WriteLine("Enter the second matrix:");
                int[,] secondMatrix = new int[rowsSecondMatrix, columnsSecondMatrix];

                for (rowsIndex = 0; rowsIndex < rowsSecondMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsSecondMatrix; columnsIndex++)
                    {
                        secondMatrix[rowsIndex, columnsIndex] = int.Parse(Console.ReadLine());
                    }
                }

                Console.WriteLine("The second matrix is:");

                for (rowsIndex = 0; rowsIndex < rowsSecondMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsSecondMatrix; columnsIndex++)
                    {
                        Console.Write(secondMatrix[rowsIndex, columnsIndex] + " ");
                    }
                    Console.WriteLine();
                }

                int[,] multiplicationMatrix = new int[rowsFirstMatrix, columnsSecondMatrix];

                for (rowsIndex = 0; rowsIndex < rowsFirstMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsSecondMatrix; columnsIndex++)
                    {
                        multiplicationMatrix[rowsIndex, columnsIndex] = 0;

                        for (int newIndex = 0; newIndex < columnsFirstMatrix; newIndex++)
                        {
                            multiplicationMatrix[rowsIndex, columnsIndex] += firstMatrix[rowsIndex, newIndex] * secondMatrix[newIndex, columnsIndex];
                        }
                    }
                }
                Console.WriteLine("The multiplication matrix is:");

                for (rowsIndex = 0; rowsIndex < rowsFirstMatrix; rowsIndex++)
                {
                    for (columnsIndex = 0; columnsIndex < columnsSecondMatrix; columnsIndex++)
                    {
                        Console.Write(multiplicationMatrix[rowsIndex, columnsIndex] + " ");
                    }
                    Console.WriteLine();
                }
            }
            Console.ReadKey();
        }

    }
}