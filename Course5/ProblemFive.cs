﻿/*
   Create a program that returns the following from different type of collections : 
    a. maximum and minimum element
    b. sum of all elements
    c. count of prime numbers
    d. average value of the prime numbers
*/


using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Course5
{
    class ProblemFive
    {
        static void Main(string[] args)
        {
            //List<int> myList = new List<int> { 48, 35, -46, -13, 33, 25, -34, 1022, -12, 34, 71, -32, 0 };

            Console.WriteLine("NOTE: For the tasks with prime numbers, enter 2 as the first number of the list!\n");

            Console.Write("Enter the first number of the list: ");
            int firstNumber = Int32.Parse(Console.ReadLine());

            Console.Write("Enter the last number of the list: ");
            int lastNumber = Int32.Parse(Console.ReadLine());

            List<int> myList = new List<int>();

            for (int currentNumber = firstNumber; currentNumber <= lastNumber; currentNumber++)
            {
                myList.Add(currentNumber);
            }

            Console.WriteLine("\nThe list is:");
            string list = string.Join(",", myList);
            Console.Write(list);

            Console.WriteLine();

            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Find the maximum element in the list.");
                Console.WriteLine("\t2. Find the minimum element in the list.");
                Console.WriteLine("\t3. Calculate the sum of all the elements in the list.");
                Console.WriteLine("\t4. Count the prime numbers in the list.");
                Console.WriteLine("\t5. Calculate the average value of the prime numbers in the list.");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                Console.WriteLine();

                switch (choice)
                {
                    case '1':
                        MaximumElement(myList);
                        break;
                    case '2':
                        MinimumElement(myList);
                        break;
                    case '3':
                        SumOfElements(myList);
                        break;
                    case '4':
                        PrimeNumbers(myList, lastNumber);
                        break;
                    case '5':
                        AverageOfPrimeNumbers(myList, lastNumber);
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        static void MaximumElement(List<int> myList)
        {
            int maximumElement = myList.Max();
            Console.WriteLine("\nUsing Max():\nThe maximum element is {0}\n", maximumElement);

            maximumElement = (from x in myList select x).Max();
            Console.WriteLine("\nUsing query syntax and Max():\nThe maximum element is {0}\n", maximumElement);
        }

        static void MinimumElement(List<int> myList)
        {
            int minimumElement = myList.Min();
            Console.WriteLine("\nUsing Min():\nThe minimum element is {0}\n", minimumElement);

            minimumElement = (from x in myList select x).Min();
            Console.WriteLine("\nUsing query syntax and Min():\nThe minimum element is {0}\n", minimumElement);
        }

        static void SumOfElements(List<int> myList)
        {
            int sum = myList.Sum();
            Console.WriteLine("\nUsing Sum():\nThe sum of the elements is {0}\n", sum);

            sum = (from x in myList select x).Sum();
            Console.WriteLine("\nUsing query syntax and Sum():\nThe sum of the elements is {0}\n", sum);
        }

        static void PrimeNumbers(List<int> myList, int lastNumber)
        {
            for (int divisor = 2; divisor < Math.Sqrt(lastNumber); divisor++)
            {
                var primeNumbers = from number in myList where number % divisor != 0 || divisor >= number select number;

                myList = primeNumbers.ToList<int>();
            }

            Console.WriteLine("\nThe prime numbers in the list are:");
            string primes = string.Join(",", myList);
            Console.Write(primes);

            Console.WriteLine("\nThere are " + myList.Count() + " prime numbers in the list.");

            Console.WriteLine();
        }

        static void AverageOfPrimeNumbers(List<int> myList, int lastNumber)
        {
            for (int divisor = 2; divisor < Math.Sqrt(lastNumber); divisor++)
            {
                var primeNumbers = from number in myList where number % divisor != 0 || divisor >= number select number;

                myList = primeNumbers.ToList<int>();
            }

            double primesAverage = myList.Average();

            Console.WriteLine("\nThe average value of the prime numbers in the list is {0}\n", primesAverage);
        }
    }
}
