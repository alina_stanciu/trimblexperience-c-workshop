﻿/*
   Create a program that has 2 options for the user:
    1. To display a list of 4 names of teachers(when the user types “1” from keyboard)
    2. To display a list of 6 names of students(when the user types “2” from keyboard)
    (both lists will be strings).
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Course5
{
    class ProblemFour
    {
        static void Main(string[] args)
        {
            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Display a list of 4 names of teachers.");
                Console.WriteLine("\t2. Display a list of 6 names of students.");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                switch (choice)
                {
                    case '1':
                        DisplayTeachers();
                        break;
                    case '2':
                        DisplayStudents();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        static void DisplayTeachers()
        {
            var random = new Random();

            var teachersList = new List<string>
                            {"Meredith Grey",
                            "Derek Sheperd",
                            "Miranda Bailey",
                            "Richard Webber",
                            "Alex Karex",
                            "Cristina Yang",
                            "Amelia Sheperd",
                            "Owent Hunt",
                            "Teddy Altman",
                            "Jo Wilson" };

            var randomTeachersList = new List<string>();

            Console.WriteLine("\nList of teachers:");

            do
            {
                int randomIndex = random.Next(teachersList.Count);

                while (!randomTeachersList.Contains(teachersList[randomIndex]))
                {
                    randomTeachersList.Add(teachersList[randomIndex]);
                    Console.WriteLine(teachersList[randomIndex]);
                }
            } while (randomTeachersList.Count < 4);

            Console.WriteLine();
        }

        static void DisplayStudents()
        {
            var random = new Random();

            var studentsList = new List<string>
                            {"Tyrel Wellick",
                            "Izzie Stevens",
                            "George O'Malley",
                            "Arizona Robbins",
                            "Callipso Torres",
                            "Lexie Grey",
                            "Mark Sloan",
                            "Robert Melandez",
                            "Jessica Alvez" };

            var randomStudentsList = new List<string>();

            Console.WriteLine("\nList of students:");

            do
            {
                int randomIndex = random.Next(studentsList.Count);

                while (!randomStudentsList.Contains(studentsList[randomIndex]))
                {
                    randomStudentsList.Add(studentsList[randomIndex]);
                    Console.WriteLine(studentsList[randomIndex]);
                }
            } while (randomStudentsList.Count < 6);

            Console.WriteLine();
        }
    }
}