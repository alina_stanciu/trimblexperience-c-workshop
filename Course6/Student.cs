﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course6
{
    class Student : Person //derived class - child
    {
        public int Grade;

        public Student (string Name, string Surname, int Age, int ID, int Grade): base(Name, Surname, Age, ID)
        {
            this.Grade = Grade;
        }

        public override string ToString()
        {
            return $"\n{base.ToString()}, {nameof(Grade)}: {Grade}";
        }

        public void CareerPath()
        {
            Console.WriteLine($"\n{Name} {Surname} is learning for his/her Bachelor's Degree.", Name, Surname);
        }
    }
}