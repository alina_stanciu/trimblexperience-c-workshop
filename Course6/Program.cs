﻿/*
    Create a class representing a person. It should be characterized by:
        Name, Surname, Age and Identifier
    Derive the Person class in two other classes: Teacher and Student, each containing their own unique properties.

    Create a program that has 2 options for the user:
        To display a list of 4 teachers objects (when the user types "1" from keyboard).
        To display a list of 6 students objects (when the user types "2" from keyboard).

    Define any two different methods with the same name for each class.
        maybe a CareerPath method: a teacher can learn for his doctor’s degree, while a student must learn for his bachelor’s
        the output should be displayed in console.
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Course6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Teacher> teachersList = new List<Teacher>();
            teachersList.Add(new Teacher("Ellen", "Pompeo", 46, 1, "Stanford University"));
            teachersList.Add(new Teacher("Patrick", "Dempsey", 51, 2, "Oxford University"));
            teachersList.Add(new Teacher("Kevin", "McKidd", 48, 3, "Ohio State University"));
            teachersList.Add(new Teacher("Camilla", "Luddington", 54, 4, "University of Central Florida"));

            List<Student> studentsList = new List<Student>();
            studentsList.Add(new Student("Elliot", "Anderson", 21, 1, 10));
            studentsList.Add(new Student("Tyrel", "Wellick", 23, 2, 8));
            studentsList.Add(new Student("Darlene", "Kurta", 19, 3, 7));
            studentsList.Add(new Student("Dominique", "Alvez", 22, 4, 4));
            studentsList.Add(new Student("Ozgecan", "Ayili", 20, 5, 6));
            studentsList.Add(new Student("Johannes", "Spaas", 24, 6, 10));

            char choice;

            while (true)
            {
                Console.WriteLine("Select one of the following tasks to perform:");
                Console.WriteLine("\t1. Display teachers.");
                Console.WriteLine("\t2. Display students.");
                Console.WriteLine("\t3. Career paths for teachers.");
                Console.WriteLine("\t4. Career paths for students.");
                Console.Write("Choose your task or press any other key to exit: ");

                choice = Console.ReadKey().KeyChar;

                switch (choice)
                {
                    case '1':
                        foreach (Teacher teacher in teachersList)
                        {
                            Console.WriteLine(teacher.ToString());
                        }
                        Console.WriteLine();
                        break;
                    case '2':
                        foreach (Student student in studentsList)
                        {
                            Console.WriteLine(student.ToString());
                        }
                        Console.WriteLine();
                        break;
                    case '3':
                        teachersList[0].CareerPath();
                        teachersList[1].CareerPath();
                        teachersList[2].CareerPath();
                        teachersList[3].CareerPath();
                        Console.WriteLine();
                        break;
                    case '4':
                        studentsList[0].CareerPath();
                        studentsList[1].CareerPath();
                        studentsList[2].CareerPath();
                        studentsList[3].CareerPath();
                        studentsList[4].CareerPath();
                        studentsList[5].CareerPath();
                        Console.WriteLine();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}