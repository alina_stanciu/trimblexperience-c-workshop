﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Course6
{
    class Teacher: Person //derived class - child
    {
        public string College;

        public Teacher(string Name, string Surname, int Age, int ID, string College): base(Name, Surname, Age, ID)
        {
            this.College = College;
        }

        public override string ToString()
        {
            return $"\n{base.ToString()}, {nameof(College)}: {College}";
        }

        public void CareerPath()
        {
            Console.WriteLine($"\n{Name} {Surname} is learning for his/her PHD.", Name, Surname);
        }
    }
}